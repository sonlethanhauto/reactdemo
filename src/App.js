
import {Navbar, NavbarBrand} from 'reactstrap'
import './App.css';
import StaffList from './components/StaffListComponent';
import { STAFFS } from './shared/staffs.jsx';

function App() {
  return (
    <div>
      <Navbar dark color='primary'>
        <div className='container m-3'>
          <NavbarBrand href='#'>Ứng dụng quản lý nhân sự V1.0</NavbarBrand>
        </div>
      </Navbar>
      <StaffList Staffs = {STAFFS} />
    </div>
  );
}

export default App;
