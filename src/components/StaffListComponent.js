import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import dateFormat from 'dateformat';
import './StaffListComponent.css';
class StaffList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedStaff: null
        }
    }
    onChangeSelectedStaff(Staff) {
        this.setState({
            selectedStaff: Staff
        });
    }

    renderStaff(Staff) {
        if (Staff) {
            return (
                <div className='bg-light border'>
                    <h3>Họ và tên: {Staff.name}</h3>
                    <p>Ngày Sinh: { dateFormat(Staff.doB, "dd/mm/yyyy") }</p>
                    <p>Ngày vào công ty: { dateFormat(Staff.startDate, "dd/mm/yyyy") }</p>
                    <p>Phòng ban: {Staff.department.name}</p>
                    <p>Số ngày nghỉ còn lại: {Staff.annualLeave}</p>
                    <p>Số ngày đã làm thêm: {Staff.overTime}</p>
                </div>
            );
        } else {
            return (
                <p>Bấm vào tên nhân viên để xem thông tin</p>
            );
        }
    }

    render() {
        const staffList=this.props.Staffs.map((Staff) => {
            return (
                <div className='mt-2'>    
                    <Col 
                        key={Staff.id} 
                        className = "bg-light border cursor"
                        onClick={() => this.onChangeSelectedStaff(Staff)}
                    >
                        {Staff.name}
                    </Col>
                </div>
            );
        })
        return (
            <div className='ms-3'>
                <Row
                    sm={1}
                    md={2}
                    lg={3} 
                >
                    {staffList}
                </Row>
                <div className='mt-2'>
                    {this.renderStaff(this.state.selectedStaff)}
                </div>
            </div>
        );
    }
}
export default StaffList;